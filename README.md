Flexible Tree Search
====================

A flexible tree search framework in C++.

Author: Charly Lersteau

This framework is designed to help implementing:

- Branch & Bound
- Beam Search
- A*
- Iterative Deepening

with customizable branching rules and search strategies.

# Instructions

Compile using CMake:

```bash
mkdir build
cd build/
cmake ..
make
```

Then run:

```bash
./examples/knapsack/knapsack01 FILE
```

# Architecture

This repository implements a simple Branch & Bound for the 0-1 Knapsack Problem.

Problem representation:

- `KnapsackModel<ProfitT, WeightT>`: Model for the Knapsack Problem.
- `KnapsackContext<ProfitT, WeightT, ValueT>`: Knapsack Problem characteristics.
- `KnapsackNode<ProfitT, WeightT, ValueT>`: Data for Knapsack subproblems. Must derive `Node`.
- `KnapsackBranch<ProfitT, WeightT, ValueT>`: Binary branching rule for the Knapsack Problem.

Algorithm representation:

- `TreeSearch`: Implements a flexible Tree Search algorithm.
- `NodeBase`: Base class for Tree Search nodes.

Search strategies:

- `BestFirstSearch`: Best First Search.
- `BreadthFirstSearch`: Breadth First Search.
- `DepthFirstSearch`: Depth First Search.
- `CyclicBestFirstSearch`: Cyclic Best First Search.
- `BasicSearch<CompareF>`: Base class to implement simple search strategies based on an ordered set.

Others:

- `Signal<R(Args...)>`: A signal/slot class to emit events.

# Datasets

- [`KNAPSACK_01`](https://people.sc.fsu.edu/~jburkardt/datasets/knapsack_01/knapsack_01.html): instances from 10 to 24 items
- [`Pisinger`](http://hjemmesider.diku.dk/~pisinger/codes.html): larger instances from 20 to 10000 items

# Instance format

```
n
p_1 ... p_n
w_1 ... w_n
c
```

Where

- `n` is the number of items
- `p_i` is the profit of item `i`
- `w_i` is the weight of item `i`
- `c` is the capacity

# Scripts

- `knapsack01_scip.py`: Solve a Knapsack instance using the MIP solver [SCIP](https://scip.zib.de/).

# References

- [Kolesar, Peter. "A branch and bound algorithm for the knapsack problem." Management Science 13, no. 9 (May 1967): 723-735.](https://www8.gsb.columbia.edu/researcharchive/articles/4407)
- [Morrison et al. (2016). "Branch-and-bound algorithms: A survey of recent advances in searching, branching, and pruning"](https://doi.org/10.1016/j.disopt.2016.01.005)

## License

This software is licensed under the GPLv3.
Please see the `LICENSE` file for further information.
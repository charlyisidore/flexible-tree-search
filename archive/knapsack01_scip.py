#!/usr/bin/env python3

# Solve a Knapsack Problem model using SCIP

import argparse
import os
import re
import subprocess
import tempfile

# Parse command line arguments
parser = argparse.ArgumentParser(
    description="Solve a Knapsack Problem model using SCIP"
)
parser.add_argument("filename", type=str, help="Path to the model")

args = parser.parse_args()
filename = args.filename

if not os.path.isfile(filename):
    print('Error: "{}" is not a valid file'.format(filename))
    exit()

# Open file
with open(filename, mode="r") as f:

    # Read file contents
    content = f.read()
    values = re.split("[ \t\r\n]+", content)
    values = [v for v in values if v != ""]

    n = int(values[0])

    if len(values) < 2 * n + 2:
        print("Error: Invalid file format")
        exit(0)

    # Make parameter strings
    profits = ", ".join(["<{}> {}".format(i + 1, values[i + 1]) for i in range(n)])
    weights = ", ".join(["<{}> {}".format(i + 1, values[i + 1 + n]) for i in range(n)])
    capacity = values[2 * n + 1]

    # Build the model
    model = """set Items := {{ 1 .. {} }};
param profits[Items] := {};
param weights[Items] := {};
param capacity := {};
var x[Items] binary;
maximize profit : sum <i> in Items : profits[i] * x[i];
subto cons: sum <i> in Items : weights[i] * x[i] <= capacity;
""".format(
        n, profits, weights, capacity
    )

    # Write the model in a temporary file
    tmp = tempfile.NamedTemporaryFile(mode="w", suffix=".zpl", delete=False)
    tmp.write(model)
    tmp.close()

    # Run SCIP
    cmd = "scip -f {}".format(tmp.name)
    result = subprocess.run(cmd, shell=True)
    os.unlink(tmp.name)

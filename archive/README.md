Flexible Tree Search
====================

A flexible tree search framework in C++.

Author: Charly Lersteau

This framework is designed to help implementing:
- Branch & Bound
- Beam Search
- A*
- Iterative Deepening
with customizable branching rules and search strategies.

# Instructions

Compile:

```bash
make
```

Then run:

```bash
./knapsack01 FILE
```

# Architecture

This repository implements a simple Branch & Bound for the 0-1 Knapsack Problem.

Problem representation:

- `Knapsack<ProfitT, WeightT, ValueT>`: Knapsack Problem characteristics.
- `KnapsackNode<ProfitT, WeightT, ValueT>`: Data for Knapsack subproblems. Must derive `Node`.
- `KnapsackModel<ProfitT, WeightT>`: Model for the Knapsack Problem.

Algorithm representation:

- `TreeSearch<TraitsT, BranchT, SearchT>`: Implements a flexible Tree Search algorithm.
- `KnapsackBranch<TraitsT>`: Binary branching rule for the Knapsack Problem.
- `BestFirstSearch<TraitsT>`: Best First Search.
- `BreadthFirstSearch<TraitsT>`: Breadth First Search.
- `DepthFirstSearch<TraitsT>`: Depth First Search.
- `CyclicBestFirstSearch<TraitsT>`: Cyclic Best First Search.

Others:

- `BasicSearch<TraitsT, CompareF>`: Base class to implement simple search strategies based on an ordered set.
- `Node`: Base class for Tree Search nodes.
- `Signal<R(Args...)>`: A signal/slot class to emit events.

# Datasets

- [`KNAPSACK_01`](https://people.sc.fsu.edu/~jburkardt/datasets/knapsack_01/knapsack_01.html): instances from 10 to 24 items
- [`Pisinger`](http://hjemmesider.diku.dk/~pisinger/codes.html): larger instances from 20 to 10000 items

# Instance format

```
n
p_1 ... p_n
w_1 ... w_n
c
```

Where

- `n` is the number of items
- `p_i` is the profit of item `i`
- `w_i` is the weight of item `i`
- `c` is the capacity

# Scripts

- `knapsack01_scip.py`: Solve a Knapsack instance using the MIP solver [SCIP](https://scip.zib.de/).


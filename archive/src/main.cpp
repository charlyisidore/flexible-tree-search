/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "best_first_search.hpp"
#include "breadth_first_search.hpp"
#include "cyclic_best_first_search.hpp"
#include "depth_first_search.hpp"
#include "knapsack.hpp"
#include "knapsack_branch.hpp"
#include "tree_search.hpp"
#include <chrono>
#include <fstream>
#include <iostream>

// Display all events
template <typename TreeSearchT>
void debug_events(TreeSearchT &ts)
{
	ts.events.init.connect([](auto &ts) {
		std::clog << "init" << std::endl;
	});

	ts.events.exit.connect([](auto &ts) {
		std::clog << "exit" << std::endl;
	});

	ts.events.node_selected.connect([](auto &ts, const auto &node) {
		std::clog << "node_selected " << node << std::endl;
	});

	ts.events.node_pushed.connect([](auto &ts, const auto &node) {
		std::clog << "  node_pushed    " << node << std::endl;
	});

	ts.events.node_candidate.connect([](auto &ts, const auto &node) {
		std::clog << "    node_candidate " << node << std::endl;
	});

	ts.events.node_pruned.connect([](auto &ts, const auto &node) {
		std::clog << "    node_pruned    " << node << std::endl;
	});

	ts.events.node_terminal.connect([](auto &ts, const auto &node) {
		std::clog << "    node_terminal  " << node << std::endl;
	});

	ts.events.node_improving.connect([](auto &ts, const auto &node) {
		std::clog << "> node_improving " << node << std::endl;
	});
}

// Show the tree
template <typename TreeSearchT>
void show_tree(TreeSearchT &ts)
{
	ts.events.node_pushed.connect([](auto &ts, const auto &node) {
		std::clog << std::string(node.depth(), ' ') << node << std::endl;
	});
}

// Display progress information
template <typename TreeSearchT>
void progress_events(TreeSearchT &ts)
{
	ts.events.node_pushed.connect([](auto &ts, const auto &node) {
		if (ts.node_count() % 100000 == 0)
		{
			std::clog << "[#nodes=" << ts.n_nodes() << "|#total=" << ts.node_count() << "]" << std::endl;
		}
	});

	ts.events.node_improving.connect([](auto &ts, const auto &node) {
		std::clog << "[#nodes=" << ts.n_nodes() << "|#total=" << ts.node_count() << "] lb=" << node.lower_bound << std::endl;
	});
}

int main(int argc, char *argv[])
{
	if (argc <= 1)
	{
		std::cout << "Usage: " << argv[0] << " FILE" << std::endl;
		return 0;
	}

	typedef int Profit;
	typedef int Weight;
	typedef Knapsack<Profit, Weight> Traits;
	typedef typename Traits::Model Model;

	// Read the instance

	std::ifstream file;
	Model model;

	file.open(argv[1]);
	if (!file.is_open())
	{
		std::cerr << "Error opening file" << std::endl;
		return 0;
	}
	model.read(file);

	// Run the algorithm

	std::chrono::time_point<std::chrono::steady_clock> begin_time, end_time;
	std::chrono::duration<double> elapsed_time;

	auto ts = make_tree_search(
		Traits(model),
		KnapsackBranch<Traits>(),
		BestFirstSearch<Traits>());

	//show_tree(ts);
	//debug_events(ts);
	progress_events(ts);

	begin_time = std::chrono::steady_clock::now();
	ts.run();
	end_time = std::chrono::steady_clock::now();
	elapsed_time = end_time - begin_time;

	// Show solutions

	std::cout << std::endl;
	std::size_t i = 0;
	for (auto node : ts.best())
	{
		++i;
		auto sol = ts.traits().solution(node);
		auto value = model.evaluate(sol);
		std::cout << "Solution " << i << ":" << std::endl;
		// std::cout << "  values:";
		// for (auto b : sol)
		// {
		// 	std::cout << " " << b;
		// }
		std::cout << "  items:";
		for (std::size_t j = 0; j < model.n_items(); ++j)
		{
			if (sol[j])
			{
				std::cout << " " << j + 1;
			}
		}
		std::cout << std::endl;
		std::cout << "  profit: " << value << std::endl;
	}

	// Summary

	std::cout << std::endl;
	std::cout << "Nodes explored: " << ts.node_count() << std::endl;
	std::cout << "Time: " << elapsed_time.count() << " s" << std::endl;

	return 0;
}

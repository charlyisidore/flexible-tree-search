/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef KNAPSACK_BRANCH_HPP
#define KNAPSACK_BRANCH_HPP

#include <functional>
#include <optional>

/**
 * Branching rule for the Knapsack Problem.
 * 
 * Binary branching on the fractional variable.
 */
template <typename TraitsT>
struct KnapsackBranch
{
	/**
	 * Traits type definition.
	 */
	typedef TraitsT Traits;

	/**
	 * Node type definition.
	 */
	typedef typename Traits::Node Node;

	/**
	 * Initialize the object.
	 */
	template <typename TreeSearchT>
	void initialize(TreeSearchT &ts)
	{
		_traits = ts.traits();
	}

	/**
	 * Create child nodes from a parent node.
	 */
	template <typename InserterF>
	void operator()(const Node &parent, InserterF inserter)
	{
		auto i = parent.critical_index;

		// Select the critical item
		Node left = parent;
		_traits->get().fix(left, i, true);
		if (_traits->get().evaluate(left))
		{
			inserter(std::move(left));
		}

		// Discard the critical item
		Node right = parent;
		_traits->get().fix(right, i, false);
		if (_traits->get().evaluate(right))
		{
			inserter(std::move(right));
		}
	}

private:
	std::optional<std::reference_wrapper<const Traits>> _traits;
};

#endif

/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TREE_SEARCH_HPP
#define TREE_SEARCH_HPP

#include "signal.hpp"
#include <iostream>
#include <vector>

template <typename TraitsT, typename BranchT, typename SearchT>
class TreeSearch
{
public:
    /**
     * Problem traits type definition.
     */
    typedef TraitsT Traits;

    /**
     * Node branching rule type definition.
     */
    typedef BranchT Branch;

    /**
     * Node search rule type definition.
     */
    typedef SearchT Search;

    /**
     * Node type definition.
     */
    typedef typename Traits::Node Node;

    /**
     * Node pool type definition.
     */
    typedef std::vector<Node> NodePool;

    /**
     * Events type definition.
     */
    typedef struct
    {
        Signal<void(TreeSearch &)> init;
        Signal<void(TreeSearch &)> exit;
        Signal<void(TreeSearch &, const Node &)> node_selected;
        Signal<void(TreeSearch &, const Node &)> node_pushed;
        Signal<void(TreeSearch &, const Node &)> node_candidate;
        Signal<void(TreeSearch &, const Node &)> node_pruned;
        Signal<void(TreeSearch &, const Node &)> node_terminal;
        Signal<void(TreeSearch &, const Node &)> node_improving;
    } Events;

    /**
     * Events definition.
     */
    Events events;

    /**
     * Constructor.
     */
    TreeSearch(const Traits &t, const Branch &b, const Search &s) : _traits(t),
                                                                    _branch(b),
                                                                    _search(s)
    {
    }

    /**
     * Constructor.
     */
    TreeSearch(Traits &&t, Branch &&b, Search &&s) : _traits(std::move(t)),
                                                     _branch(std::move(b)),
                                                     _search(std::move(s))
    {
    }

    /**
     * Get problem traits.
     */
    const Traits &traits() const
    {
        return _traits;
    }

    /**
     * Get actual number of nodes.
     */
    std::size_t n_nodes() const
    {
        return _search.size();
    }

    /**
     * Get the total node count.
     */
    std::size_t node_count() const
    {
        return _node_count;
    }

    /**
     * Run the algorithm loop.
     */
    void run()
    {
        // Run without additional termination condition
        run([](auto &) { return false; });
    }

    /**
     * Run the algorithm loop.
     */
    template <typename TerminationF>
    void run(TerminationF termination)
    {
        _branch.initialize(*this);
        _search.initialize(*this);

        events.init(*this);

        // Insert root node
        Node root = _traits.root();
        root.set_index(_node_count++);
        root.set_depth(0);
        _push(std::move(root));

        // Start loop
        while (!_search.empty() && !termination(*this))
        {
            // Get the top-priority node and remove it from the queue
            auto it = _search.top();
            Node parent = *it;
            _search.erase(it);

            // Helps the branching function to add nodes to the queue
            auto inserter = [this, parent](Node &&child) {
                child.set_index(_node_count++);
                child.set_depth(parent.depth() + 1);
                _push(std::move(child));
            };

            // Notify that a node is selected for branching
            events.node_selected(*this, parent);

            // Create child nodes
            _branch(parent, inserter);
        }

        events.exit(*this);
    }

    /**
     * Get best solutions.
     */
    const NodePool &best() const
    {
        return _best;
    }

private:
    /**
     * Add a node to the queue.
     */
    void _push(Node &&node)
    {
        events.node_pushed(*this, node);

        // Promising node
        if (_best.empty() || !_traits.dominates(_best.front(), node))
        {
            // Node is a leaf
            if (_traits.is_terminal(node))
            {
                events.node_terminal(*this, node);

                if (!_best.empty())
                {
                    for (const auto &n : _best)
                    {
                        events.node_pruned(*this, n);
                    }
                    _best.clear();
                }

                _search.filter(node,
                               [this](auto n) {
                                   events.node_pruned(*this, n);
                               });

                // New best-so-far node
                events.node_improving(*this, node);
                _best.push_back(std::move(node));
            }
            // Promising node having child nodes
            else
            {
                events.node_candidate(*this, node);
                _search.push(std::move(node));
            }
        }
        // Dominated node
        else
        {
            events.node_pruned(*this, node);
        }
    }

    Traits _traits;
    Branch _branch;
    Search _search;
    NodePool _best;
    std::size_t _node_count = 0;
};

/**
 * Helper function to build a TreeSearch object.
 */
template <typename TraitsT, typename BranchT, typename SearchT>
auto make_tree_search(TraitsT &&t, BranchT &&b, SearchT &&s)
{
    return TreeSearch<TraitsT, BranchT, SearchT>(t, b, s);
}

#endif

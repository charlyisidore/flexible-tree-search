/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BASIC_SEARCH_HPP
#define BASIC_SEARCH_HPP

#include <functional>
#include <optional>
#include <set>

/**
 * Base class for basic search strategies such as BFS, BrFS and DFS.
 */
template <typename TraitsT, typename CompareF>
class BasicSearch
{
public:
    /**
     * Problem traits type definition.
     */
    typedef TraitsT Traits;

    /**
     * Node type definition.
     */
    typedef typename Traits::Node Node;

    /**
     * Comparator type definition.
     */
    typedef CompareF Compare;

    /**
     * Container type definition.
     */
    typedef std::set<Node, Compare> Container;

    /**
     * Pointer type definition.
     */
    typedef typename Container::iterator Pointer;

    /**
     * Initialize the object.
     */
    template <typename TreeSearchT>
    void initialize(TreeSearchT &ts)
    {
        _queue = Container(Compare(ts.traits()));
        _traits = ts.traits();
    }

    /**
     * Check whether the queue is empty.
     */
    bool empty() const
    {
        return _queue.empty();
    }

    /**
     * Get the current size of the queue.
     */
    std::size_t size() const
    {
        return _queue.size();
    }

    /**
     * Select the highest priority node.
     */
    Pointer top()
    {
        return _queue.begin();
    }

    /**
     * Add a node to the queue.
     */
    Pointer push(Node &&node)
    {
        return _queue.insert(std::move(node)).first;
    }

    /**
     * Remove a node from the queue.
     */
    void erase(Pointer ptr)
    {
        _queue.erase(ptr);
    }

    /**
     * Filter nodes dominated by the given one.
     */
    template <typename CallbackF>
    void filter(const Node &node, CallbackF callback)
    {
        for (auto it = std::begin(_queue); it != std::end(_queue);)
        {
            if (_traits->get().dominates(node, *it))
            {
                callback(*it);
                it = _queue.erase(it);
            }
            else
            {
                ++it;
            }
        }
    }

private:
    Container _queue;
    std::optional<std::reference_wrapper<const Traits>> _traits;
};

#endif

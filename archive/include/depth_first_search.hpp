/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DEPTH_FIRST_SEARCH_HPP
#define DEPTH_FIRST_SEARCH_HPP

#include "basic_search.hpp"
#include <functional>
#include <optional>

/**
 * Comparator for Depth First Search.
 */
template <typename TraitsT>
struct DepthFirstSearchCompare
{
    typedef TraitsT Traits;
    typedef typename Traits::Node Node;

    DepthFirstSearchCompare() : traits(std::nullopt) {}
    DepthFirstSearchCompare(const Traits &traits) : traits(traits) {}

    bool operator()(const Node &node1, const Node &node2)
    {
        return node1.depth() > node2.depth() ||
               (node1.depth() == node2.depth() &&
                (traits->get().compare(node1, node2) ||
                 (!traits->get().compare(node2, node1) &&
                  node1.index() < node2.index())));
    }

    std::optional<std::reference_wrapper<const Traits>> traits;
};

/**
 * Depth First Search.
 */
template <typename TraitsT>
using DepthFirstSearch = BasicSearch<TraitsT, DepthFirstSearchCompare<TraitsT>>;

#endif

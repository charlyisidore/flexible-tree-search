/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SIGNAL_HPP
#define SIGNAL_HPP

#include <functional>
#include <vector>

template <typename T>
class Signal;

/**
 * A minimal signal/slot mechanism.
 */
template <typename R, typename... Args>
class Signal<R(Args...)>
{
public:
    typedef std::function<R(Args...)> Slot;

    /**
	 * Constructor.
	 */
    Signal() = default;

    /**
	 * Connect a slot to the signal emitter.
	 */
    void connect(const Slot &slot);

    /**
	 * Emit a signal.
	 */
    void operator()(Args... args) const;

private:
    std::vector<Slot> _slots;
};

////////////////////////////////////////////////////////////////////////////////

template <typename R, typename... Args>
inline void Signal<R(Args...)>::connect(const Slot &slot)
{
    _slots.push_back(slot);
}

template <typename R, typename... Args>
inline void Signal<R(Args...)>::operator()(Args... args) const
{
    for (auto slot : _slots)
    {
        slot(args...);
    }
}

#endif

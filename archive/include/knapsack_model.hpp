/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef KNAPSACK_MODEL_HPP
#define KNAPSACK_MODEL_HPP

#include <algorithm>
#include <iostream>
#include <numeric>
#include <stdexcept>
#include <vector>

/**
 * Model description for the Knapsack Problem.
 */
template <typename ProfitT, typename WeightT>
class KnapsackModel
{
public:
    /**
     * Profit type definition.
     */
    typedef ProfitT Profit;

    /**
     * Weight type definition.
     */
    typedef WeightT Weight;

    /**
     * Solution type definition.
     */
    typedef std::vector<bool> Solution;

    /**
     * Get the number of items.
     */
    std::size_t n_items() const
    {
        return _profits.size();
    }

    /**
     * Get the profit of a given item.
     */
    Profit profit(std::size_t i) const
    {
        return _profits[_indices[i]];
    }

    /**
     * Get the weight of a given item.
     */
    Weight weight(std::size_t i) const
    {
        return _weights[_indices[i]];
    }

    /**
     * Get the capacity of the knapsack.
     */
    Weight capacity() const
    {
        return _capacity;
    }

    /**
     * Get the profit of a given item.
     */
    Profit ordered_profit(std::size_t i) const
    {
        return _profits[i];
    }

    /**
     * Get the weight of a given ordered item.
     */
    Weight ordered_weight(std::size_t i) const
    {
        return _weights[i];
    }

    /**
     * Get the ordered index of a given original item.
     */
    std::size_t ordered_index(std::size_t i) const
    {
        return _indices[i];
    }

    /**
     * Evaluate a solution.
     */
    Profit evaluate(const Solution &solution)
    {
        if (solution.size() != n_items())
        {
            throw std::runtime_error("Invalid solution");
        }

        Profit p = 0;

        for (std::size_t i = 0; i < n_items(); ++i)
        {
            if (solution[i])
            {
                p += profit(i);
            }
        }
        return p;
    }

    /**
     * Read an instance.
     */
    void read(std::istream &is = std::cin)
    {
        std::size_t n;

        is >> n;

        _profits.resize(n);
        _weights.resize(n);

        for (auto &p : _profits)
        {
            is >> p;
        }

        for (auto &w : _weights)
        {
            is >> w;
        }

        is >> _capacity;

        // Sort items by decreasing profit/weight ratio
        _sort();
    }

    /**
     * Write an instance.
     */
    void write(std::ostream &os = std::cout)
    {
        os << n_items() << std::endl;

        for (std::size_t i = 0; i < n_items(); ++i)
        {
            if (i > 0)
            {
                os << ' ';
            }
            os << profit(i);
        }
        os << std::endl;

        for (std::size_t i = 0; i < n_items(); ++i)
        {
            if (i > 0)
            {
                os << ' ';
            }
            os << weight(i);
        }
        os << std::endl;

        os << capacity() << std::endl;
    }

private:
    /**
     * Sort indices by decreasing profit/weight ratio.
     */
    void _sort()
    {
        std::vector<std::size_t> idx(n_items());

        // Create a sequence 0, 1, ..., n-1
        std::iota(idx.begin(), idx.end(), 0);

        // Sort indices
        std::sort(idx.begin(), idx.end(), [this](auto i, auto j) {
            double d = double(_profits[j]) / double(_weights[j]) - double(_profits[i]) / double(_weights[i]);
            return d < 0 || (!(0 < d) && (_profits[j] < _profits[i]));
        });

        auto p = _profits;
        auto w = _weights;

        _indices.resize(n_items());

        for (std::size_t i = 0; i < n_items(); ++i)
        {
            std::size_t j = idx[i];
            _profits[i] = p[j];
            _weights[i] = w[j];
            _indices[j] = i;
        }
    }

    std::vector<Profit> _profits;      // List of profits.
    std::vector<Weight> _weights;      // List of weights.
    Weight _capacity;                  // Knapsack capacity.
    std::vector<std::size_t> _indices; // List of indices ordered by decreasing profit/weight ratio.
};

#endif

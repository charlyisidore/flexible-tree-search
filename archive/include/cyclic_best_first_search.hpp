/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CYCLIC_BEST_FIRST_SEARCH_HPP
#define CYCLIC_BEST_FIRST_SEARCH_HPP

#include <functional>
#include <optional>
#include <set>
#include <stdexcept>
#include <vector>

/**
 * Cyclic Best First Search.
 */
template <typename TraitsT>
class CyclicBestFirstSearch
{
public:
    /**
     * Problem traits type definition.
     */
    typedef TraitsT Traits;

    /**
     * Node type definition.
     */
    typedef typename Traits::Node Node;

    /**
     * Compare two nodes.
     */
    struct Compare
    {
        Compare() : traits(std::nullopt) {}
        Compare(const Traits &traits) : traits(traits) {}

        bool operator()(const Node &node1, const Node &node2)
        {
            return traits->get().compare(node1, node2) ||
                   (!traits->get().compare(node2, node1) &&
                    node1.index() < node2.index());
        }

        std::optional<std::reference_wrapper<const Traits>> traits;
    };

    /**
     * Container type definition.
     */
    typedef std::vector<std::set<Node, Compare>> Container;

    /**
     * Pointer type definition.
     */
    struct Pointer
    {
        typename std::set<Node, Compare>::iterator iterator;
        std::size_t depth;

        const Node &operator*() const
        {
            return *iterator;
        }
    };

    /**
     * Initialize the object.
     */
    template <typename TreeSearchT>
    void initialize(TreeSearchT &ts)
    {
        _queue.clear();
        _traits = ts.traits();
    }

    /**
     * Check whether the queue is empty.
     */
    bool empty() const
    {
        for (std::size_t d = 0; d < _queue.size(); ++d)
        {
            if (!_queue[d].empty())
            {
                return false;
            }
        }
        return true;
    }

    /**
     * Get the current size of the queue.
     */
    std::size_t size() const
    {
        std::size_t s = 0;
        for (std::size_t d = 0; d < _queue.size(); ++d)
        {
            s += _queue[d].size();
        }
        return s;
    }

    /**
     * Select the highest priority node.
     */
    Pointer top()
    {
        if (_depth >= _queue.size() || _queue[_depth].empty())
        {
            _depth = 0;
        }

        while (_depth < _queue.size() && _queue[_depth].empty())
        {
            ++_depth;
        }

        if (_depth >= _queue.size())
        {
            throw std::runtime_error("Queue is empty");
        }

        auto it = _queue[_depth].begin();
        return {it, _depth++};
    }

    /**
     * Add a node to the queue.
     */
    Pointer push(Node &&node)
    {
        if (node.depth() >= _queue.size())
        {
            _queue.resize(node.depth() + 1, std::set<Node, Compare>(Compare(_traits->get())));
        }
        auto it = _queue[node.depth()].insert(std::move(node)).first;
        return {it, node.depth()};
    }

    /**
     * Remove a node from the queue.
     */
    void erase(Pointer ptr)
    {
        _queue[ptr.depth].erase(ptr.iterator);
    }

    /**
     * Filter nodes dominated by the given one.
     */
    template <typename CallbackF>
    void filter(const Node &node, CallbackF callback)
    {
        for (std::size_t d = 0; d < _queue.size(); ++d)
        {
            for (auto it = std::begin(_queue[d]); it != std::end(_queue[d]);)
            {
                if (_traits->get().dominates(node, *it))
                {
                    callback(*it);
                    it = _queue[d].erase(it);
                }
                else
                {
                    ++it;
                }
            }
        }
    }

private:
    Container _queue;
    std::size_t _depth = 0;
    std::optional<std::reference_wrapper<const Traits>> _traits;
};

#endif

/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef KNAPSACK_HPP
#define KNAPSACK_HPP

#include "knapsack_model.hpp"
#include "knapsack_node.hpp"
#include <cmath>
#include <iostream>
#include <limits>
#include <type_traits>

/**
 * Describe Knapsack problem characteristics.
 */
template <typename ProfitT, typename WeightT, typename ValueT = double>
class Knapsack
{
public:
    /**
     * Profit type definition.
     */
    typedef ProfitT Profit;

    /**
     * Weight type definition.
     */
    typedef WeightT Weight;

    /**
     * Relaxation value type definition.
     */
    typedef ValueT Value;

    /**
     * Model type definition.
     */
    typedef KnapsackModel<Profit, Weight> Model;

    /**
     * Solution type definition.
     */
    typedef typename Model::Solution Solution;

    /**
     * Node type definition.
     */
    typedef KnapsackNode<Profit, Weight, Value> Node;

    // Get default epsilon values according to the value type
    template <typename T>
    static T _default_epsilon()
    {
        if constexpr (std::is_floating_point<T>::value)
        {
            return 1e-6;
        }
        else
        {
            return 0;
        }
    }

    /**
     * Constructor.
     */
    Knapsack(const Model &model = Model(),
             Value value_epsilon = _default_epsilon<Value>(),
             Weight weight_epsilon = _default_epsilon<Weight>())
        : _model(model),
          _value_epsilon(value_epsilon),
          _weight_epsilon(weight_epsilon)
    {
    }

    /**
     * Get the model.
     */
    const Model &model() const
    {
        return _model;
    }

    /**
     * Set the model.
     */
    void set_model(const Model &model)
    {
        _model = model;
    }

    /**
     * Set the tolerance for relaxation values.
     */
    void set_value_tolerance(Value epsilon)
    {
        _value_epsilon = epsilon;
    }

    /**
     * Set the tolerance for weights.
     */
    void set_weight_tolerance(Weight epsilon)
    {
        _weight_epsilon = epsilon;
    }

    /**
     * Build the root node.
     */
    Node root() const
    {
        Node root;
        root.selected.resize(_model.n_items(), false);
        root.fixed.resize(_model.n_items(), false);
        evaluate(root);
        return root;
    }

    /**
     * Check whether given node is valid (i.e. is not guaranteed infeasible).
     */
    bool is_valid(const Node &node) const
    {
        return node.weight <= _model.capacity() + _weight_epsilon;
    }

    /**
     * Check whether given node is a leaf (i.e. is feasible).
     */
    bool is_terminal(const Node &node) const
    {
        return node.lower_bound + _value_epsilon >= node.upper_bound ||
               node.critical_index == std::numeric_limits<std::size_t>::max();
    }

    /**
     * Check whether a given node is considered more promising than another given node.
     */
    bool compare(const Node &node1, const Node &node2) const
    {
        return node1.upper_bound > node2.upper_bound;
    }

    /**
     * Check whether a given node dominates another given node.
     */
    bool dominates(const Node &node1, const Node &node2) const
    {
        return node1.lower_bound > node2.upper_bound + _value_epsilon;
    }

    /**
     * Get a solution from a node.
     */
    Solution solution(const Node &node) const
    {
        Solution sol(_model.n_items()); // vector<bool>

        for (std::size_t i = 0; i < _model.n_items(); ++i)
        {
            std::size_t j = _model.ordered_index(i);
            sol[i] = node.selected[j];
        }
        return sol;
    }

    /**
     * Fix a variable.
     */
    void fix(Node &node, std::size_t i, bool sel) const
    {
        if (sel)
        {
            node.profit += _model.ordered_profit(i);
            node.weight += _model.ordered_weight(i);
        }
        node.selected[i] = sel;
        node.fixed[i] = true;
    }

    /**
     * Solve the subproblem contained in given node.
     */
    bool evaluate(Node &node) const
    {
        if (!is_valid(node))
        {
            return false;
        }

        Weight weight = node.weight;

        node.lower_bound = node.profit;
        node.critical_index = std::numeric_limits<std::size_t>::max();

        // Solve relaxation
        for (std::size_t i = 0; i < _model.n_items(); ++i)
        {
            if (!node.fixed[i])
            {
                // Add items until it is full
                if (node.critical_index == std::numeric_limits<std::size_t>::max() &&
                    weight + _model.ordered_weight(i) <= _model.capacity() + _weight_epsilon)
                {
                    node.lower_bound += _model.ordered_profit(i);
                    weight += _model.ordered_weight(i);
                    node.selected[i] = true;
                }
                else
                {
                    node.selected[i] = false;

                    // Store the critical index
                    if (node.critical_index == std::numeric_limits<std::size_t>::max())
                    {
                        node.critical_index = i;
                    }
                }
            }
        }

        // lower_bound is the value of a feasible solution
        node.upper_bound = node.lower_bound;

        // Add a fractional item for the relaxation (upper_bound)
        if (node.critical_index != std::numeric_limits<std::size_t>::max())
        {
            node.upper_bound += double(_model.capacity() - weight) *
                                double(_model.ordered_profit(node.critical_index)) /
                                double(_model.ordered_weight(node.critical_index));

            // When all profits are integers, the upper bound can be tightened
            if constexpr (std::is_integral<Profit>::value)
            {
                node.upper_bound = std::floor(node.upper_bound);
            }
        }

        return true;
    }

private:
    Model _model;
    Value _value_epsilon = 0;
    Weight _weight_epsilon = 0;
};

#endif

/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef KNAPSACK_NODE_HPP
#define KNAPSACK_NODE_HPP

#include "node.hpp"
#include <iostream>
#include <limits>
#include <vector>

/**
 * Knapsack node type definition.
 */
template <typename ProfitT, typename WeightT, typename ValueT>
struct KnapsackNode : Node
{
    ProfitT profit = 0;                                                   // Profit of fixed selected items.
    WeightT weight = 0;                                                   // Weight of fixed selected items.
    std::vector<bool> selected;                                           // Selected items.
    std::vector<bool> fixed;                                              // Fixed items.
    ValueT lower_bound = 0;                                               // Lower bound.
    ValueT upper_bound = 0;                                               // Upper bound.
    std::size_t critical_index = std::numeric_limits<std::size_t>::max(); // Critical index.
};

/**
 * Print a given node.
 */
template <typename ProfitT, typename WeightT, typename ValueT>
inline std::ostream &operator<<(std::ostream &os, const KnapsackNode<ProfitT, WeightT, ValueT> &node)
{
    os << "[" << node.index() + 1
       << "] d=" << node.depth() + 1
       << " p=" << node.profit
       << " w=" << node.weight
       << " lb=" << node.lower_bound
       << " ub=" << node.upper_bound
       << " ci=";

    if (node.critical_index == std::numeric_limits<std::size_t>::max())
    {
        os << "*";
    }
    else
    {
        os << node.critical_index + 1;
    }

    os << " {";
    for (std::size_t i = 0; i < node.selected.size(); ++i)
    {
        if (node.fixed[i])
        {
            os << " " << i + 1 << (node.selected[i] ? "*" : "");
        }
    }
    os << " }";
    return os;
}

#endif

/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef KNAPSACK_BRANCH_HPP
#define KNAPSACK_BRANCH_HPP

#include "knapsack_context.hpp"
#include "knapsack_node.hpp"
#include "tree_search.hpp"

/**
 * Branching rule for the Knapsack Problem.
 * 
 * Binary branching on the fractional variable.
 */
template <typename ProfitT, typename WeightT, typename ValueT = double>
class KnapsackBranch
{
public:
    /**
     * Profit type definition.
     */
    typedef ProfitT Profit;

    /**
     * Weight type definition.
     */
    typedef WeightT Weight;

    /**
     * Relaxation value type definition.
     */
    typedef ValueT Value;

    /**
     * Node type definition.
     */
    typedef KnapsackNode<Profit, Weight, Value> Node;

    /**
     * Context type definition.
     */
    typedef KnapsackContext<Profit, Weight, Value> Context;

    /**
     * Initialize the object.
     */
    void initialize(TreeSearch &ts)
    {
        _ctx = ts.context();
    }

    /**
     * Create child nodes from a parent node.
     */
    void operator()(const Node &parent, TreeSearch::Inserter insert)
    {
        auto i = parent.critical_index;
        auto &ctx = _ctx->as<Context>();

        // Select the critical item
        Node left = parent;
        ctx.fix(left, i, true);
        if (ctx.evaluate(left))
        {
            insert(std::move(left));
        }

        // Discard the critical item
        Node right = parent;
        ctx.fix(right, i, false);
        if (ctx.evaluate(right))
        {
            insert(std::move(right));
        }
    }

private:
    TreeSearch::ContextPtr _ctx;
};

#endif

/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "best_first_search.hpp"
#include "breadth_first_search.hpp"
#include "cyclic_best_first_search.hpp"
#include "depth_first_search.hpp"
#include "knapsack_branch.hpp"
#include "knapsack_context.hpp"
#include "knapsack_model.hpp"
#include "tree_search.hpp"
#include <chrono>
#include <fstream>
#include <getopt.h>
#include <iostream>

static const char USAGE[] =
    R"(0-1 knapsack solver.

    Usage:
      knapsack01 [--search=<s>] [--debug] [-q] <instance>
      knapsack01 (-h | --help)

    Options:
      -h --help     Show this screen.
      --search=<s>  Specify search type (default: bfs).
      --debug       Print debug information.
      -q --quiet    Print only the solution.
)";

typedef int Profit;
typedef int Weight;
typedef KnapsackNode<Profit, Weight> Node;
typedef KnapsackModel<Profit, Weight> Model;
typedef KnapsackContext<Profit, Weight> Context;
typedef KnapsackBranch<Profit, Weight> BranchRule;

void debug_events(TreeSearch &ts)
{
    ts.events.init.connect([](auto &) {
        std::clog << "init" << std::endl;
    });

    ts.events.exit.connect([](auto &) {
        std::clog << "exit" << std::endl;
    });

    ts.events.node_selected.connect([](auto &ts, const auto &node) {
        std::clog << "node_selected ";
        ts.context()->print(node, std::clog);
        std::clog << std::endl;
    });

    ts.events.node_pushed.connect([](auto &ts, const auto &node) {
        std::clog << "  node_pushed    ";
        ts.context()->print(node, std::clog);
        std::clog << std::endl;
    });

    ts.events.node_candidate.connect([](auto &ts, const auto &node) {
        std::clog << "    node_candidate ";
        ts.context()->print(node, std::clog);
        std::clog << std::endl;
    });

    ts.events.node_pruned.connect([](auto &ts, const auto &node) {
        std::clog << "    node_pruned    ";
        ts.context()->print(node, std::clog);
        std::clog << std::endl;
    });

    ts.events.node_terminal.connect([](auto &ts, const auto &node) {
        std::clog << "    node_terminal  ";
        ts.context()->print(node, std::clog);
        std::clog << std::endl;
    });

    ts.events.node_improving.connect([](auto &ts, const auto &node) {
        std::clog << "> node_improving ";
        ts.context()->print(node, std::clog);
        std::clog << std::endl;
    });
}

void debug_tree(TreeSearch &ts)
{
    ts.events.node_pushed.connect([](auto &t, const auto &node) {
        std::clog << std::string(node.depth() * 2, ' ');
        t.context()->print(node, std::clog);
        std::clog << std::endl;
    });
}

struct ProgressLog
{
    void initialize(TreeSearch &ts)
    {
        ts.events.node_pushed.connect([this](auto &t, const NodeBase &) {
            if (t.node_count() % 100000 == 0)
            {
                std::clog << "#pending=" << t.n_nodes() << " | #total=" << t.node_count() << " | best=" << lower_bound << std::endl;
            }
        });

        ts.events.node_improving.connect([this](auto &, const NodeBase &node) {
            const auto &knode = node.as<Node>();
            if (knode.lower_bound > lower_bound)
            {
                lower_bound = knode.lower_bound;
            }
        });
    }
    double lower_bound = -std::numeric_limits<double>::infinity();
};

void progress_events(TreeSearch &ts)
{
    ts.events.node_pushed.connect([](auto &t, const auto &) {
        if (t.node_count() % 100000 == 0)
        {
            std::clog << "#nodes=" << t.n_nodes() << "|#total=" << t.node_count() << std::endl;
        }
    });

    // ts.events.node_improving.connect([](auto &t, const NodeBase &node) {
    //     std::clog << "New best: " << node.as<Node>().lower_bound << std::endl;
    // });
}

int main(int argc, char **argv)
{
    std::string instance_filename;
    std::string search_type = "bfs";
    bool debug = false;
    bool verbose = true;

    // Parse command line options

    {
        int help_flag = 0;
        int debug_flag = 0;
        int quiet_flag = 0;

        static struct ::option long_options[] = {
            {"help", no_argument, &help_flag, 1},
            {"search", required_argument, 0, 's'},
            {"debug", no_argument, &debug_flag, 1},
            {"quiet", no_argument, &quiet_flag, 1},
            {0, 0, 0, 0}};

        while (true)
        {
            int option_index = 0;

            int c = getopt_long(argc, argv, "hq", long_options, &option_index);

            if (c == -1)
            {
                break;
            }

            switch (c)
            {
                case 0:
                    break;

                case 'h':
                    help_flag = 1;
                    break;

                case 'q':
                    quiet_flag = 1;
                    break;

                case 's':
                    search_type = optarg;
                    break;

                case '?':
                    // getopt_long already printed an error message
                    std::abort();

                default:
                    std::abort();
            }
        }

        if (help_flag)
        {
            std::cout << USAGE << std::endl;
            return 0;
        }

        debug = debug_flag == 1;
        verbose = quiet_flag == 0;

        if (optind < argc)
        {
            instance_filename = argv[optind++];
        }
    }

    // Read the instance

    std::ifstream file;
    Model model;

    file.open(instance_filename);
    if (!file.is_open())
    {
        std::cerr << "Error opening file" << std::endl;
        return 0;
    }
    model.read(file);

    // Initialize the tree search

    Context ctx(model);
    BranchRule branch;
    ProgressLog progress;

    TreeSearch ts;

    ts.set_context(ctx);
    ts.set_branch_rule(branch);

    // Setup search type
    if (search_type == "bfs")
    {
        ts.set_search_rule(BestFirstSearch(ts));
    }
    else if (search_type == "dfs")
    {
        ts.set_search_rule(DepthFirstSearch(ts));
    }
    else if (search_type == "brfs")
    {
        ts.set_search_rule(BreadthFirstSearch(ts));
    }
    else if (search_type == "cbfs")
    {
        ts.set_search_rule(CyclicBestFirstSearch(ts));
    }
    else
    {
        std::cerr << "Error: unknown search type '" << search_type << "'" << std::endl;
        return 0;
    }

    if (debug)
    {
        debug_tree(ts);
    }
    else if (verbose)
    {
        progress.initialize(ts);
        //progress_events(ts);
    }

    // Run the algorithm

    std::chrono::time_point<std::chrono::steady_clock> begin_time, end_time;
    std::chrono::duration<double> elapsed_time;

    begin_time = std::chrono::steady_clock::now();

    ts.run();

    end_time = std::chrono::steady_clock::now();

    elapsed_time = end_time - begin_time;

    // Show solutions

    if (verbose)
    {
        std::cout << std::endl;
        std::size_t i = 0;
        for (const auto &node : ts.best())
        {
            ++i;
            auto sol = ts.context()->as<Context>().solution(node->as<Node>());
            auto value = model.evaluate(sol);
            std::cout << "Solution " << i << ":" << std::endl;
            // std::cout << "  values:";
            // for (auto b : sol)
            // {
            //     std::cout << " " << b;
            // }
            std::cout << "  items:";
            for (std::size_t j = 0; j < model.n_items(); ++j)
            {
                if (sol[j])
                {
                    std::cout << " " << j + 1;
                }
            }
            std::cout << std::endl;
            std::cout << "  profit: " << value << std::endl;
        }

        // Summary

        std::cout << std::endl;
        std::cout << "Nodes explored: " << ts.node_count() << std::endl;
        std::cout << "Time: " << elapsed_time.count() << " s" << std::endl;
    }
    else if (!ts.best().empty())
    {
        // Quiet mode: Display the first best solution only
        const auto &node = *std::begin(ts.best());
        auto sol = ts.context()->as<Context>().solution(node->as<Node>());
        for (std::size_t i = 0; i < model.n_items(); ++i)
        {
            if (i > 0)
            {
                std::cout << " ";
            }
            std::cout << (sol[i] ? 1 : 0);
        }
        std::cout << std::endl;
    }

    return 0;
}

#!/usr/bin/env python3
# Download and convert Pisinger's instances

import csv
import os
import requests
import tarfile
import tqdm

# import zipfile

output_path = "."

tgz_urls = [
    "http://www.diku.dk/~pisinger/smallcoeff_pisinger.tgz",
    "http://www.diku.dk/~pisinger/largecoeff_pisinger.tgz",
    "http://www.diku.dk/~pisinger/hardinstances_pisinger.tgz",
]

# Function to download a file with a progress bar
def download_url(url, path):

    print('Download "{}" to "{}"...'.format(url, path))

    r = requests.get(url, stream=True)

    total_size = int(r.headers.get("content-length", 0))
    block_size = 8096

    with open(path, "wb") as fd:

        pbar = tqdm.tqdm(total=total_size, unit="B", unit_scale=True, unit_divisor=1024)

        for chunk in r.iter_content(block_size):

            l = fd.write(chunk)
            pbar.update(l)

        pbar.close()


for url in tgz_urls:

    tgz_filename = os.path.basename(url)
    print(tgz_filename)

    if not os.path.isfile(tgz_filename):
        download_url(url, tgz_filename)

    tar = tarfile.open(tgz_filename, "r")

    for member in tar.getmembers():

        f = tar.extractfile(member)
        if f and member.name.endswith(".csv"):
            print(member.name)
            csvfile = (line.decode("utf8") for line in f)
            reader = csv.reader(csvfile)
            header = True
            instance = None
            for entry in reader:
                if header and len(entry) == 1 and not entry[0].startswith("-"):
                    # You can filter the instances here (e.g. n_items == "10000")
                    if instance:
                        with open(
                            "{}/{}.dat".format(output_path, instance), mode="w"
                        ) as fp:
                            content = "{}\n{}\n{}\n{}".format(
                                n_items, " ".join(profits), " ".join(weights), capacity
                            )
                            fp.write(content)
                        with open(
                            "{}/{}.sol".format(output_path, instance), mode="w"
                        ) as fp:
                            content = " ".join(solution)
                            fp.write(content)
                    instance = entry[0].strip()
                    n_items = 0
                    profits = []
                    weights = []
                    solution = []
                    capacity = 0
                    header = False
                elif len(entry) == 1 and entry[0].startswith("n "):
                    n_items = entry[0][2:].strip()
                elif len(entry) == 1 and entry[0].startswith("c "):
                    capacity = entry[0][2:].strip()
                elif len(entry) == 4:
                    profits.append(entry[1].strip())
                    weights.append(entry[2].strip())
                    solution.append(entry[3].strip())
                elif len(entry) == 0 or (len(entry) == 1 and entry[0] == "-----"):
                    header = True

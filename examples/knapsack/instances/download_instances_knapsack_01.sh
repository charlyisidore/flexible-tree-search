#!/bin/bash
# Download and convert KNAPSACK_01 instances
path="."
prefix="https://people.sc.fsu.edu/~jburkardt/datasets/knapsack_01"
for i in `seq -f "%02g" 1 8`
do
    p=`wget -O- "${prefix}/p${i}_p.txt"`
    w=`wget -O- "${prefix}/p${i}_w.txt"`
    c=`wget -O- "${prefix}/p${i}_c.txt"`
    s=`wget -O- "${prefix}/p${i}_s.txt"`
    n=`echo $p | wc -w`
    echo $n > "${path}/P${i}.dat"
    echo $p >> "${path}/P${i}.dat"
    echo $w >> "${path}/P${i}.dat"
    echo $c >> "${path}/P${i}.dat"
    echo $s > "${path}/P${i}.sol"
done
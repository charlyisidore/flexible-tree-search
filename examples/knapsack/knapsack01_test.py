#!/usr/bin/env python3

# Test the solver
# for f in `ls instances/*.dat`; do instance="`basename ${f/.dat}`"; echo "${instance}:" `python3 knapsack01_test.py -i "instances/${instance}.dat" -s "instances/${instance}.sol"`; done

import argparse
import os
import re
import subprocess
import time

# Parse command line arguments
parser = argparse.ArgumentParser(description="Knapsack01 test")
parser.add_argument(
    "-i", dest="instance", type=str, required=True, help="Path to the model"
)
parser.add_argument(
    "-s", dest="solution", type=str, required=True, help="Path to the solution"
)

args = parser.parse_args()
instance_filename = args.instance
solution_filename = args.solution

if not os.path.isfile(instance_filename):
    print('Error: "{}" is not a valid file'.format(instance_filename))
    exit()

if not os.path.isfile(solution_filename):
    print('Error: "{}" is not a valid file'.format(solution_filename))
    exit()


def test(instance, solution):
    # Open instance file
    with open(instance, mode="r") as fpi:

        # Read file contents
        content = fpi.read()
        values = re.split("[ \t\r\n]+", content)
        values = [v for v in values if v != ""]

        n = int(values[0])

        if len(values) < 2 * n + 2:
            print("Error: Invalid file format")
            exit(0)

        # Make parameter strings
        profits = [float(v) for v in values[1 : n + 1]]
        weights = [float(v) for v in values[n + 1 : 2 * n + 1]]
        capacity = float(values[2 * n + 1])

        # Read solution file
        with open(solution, mode="r") as fps:
            content = fps.read()
            expected_sol = re.split("[ \t\r\n]+", content)
            expected_sol = [int(v) for v in expected_sol if v != ""]
            expected_profit = sum(
                [profits[i] * expected_sol[i] for i in range(len(profits))]
            )
            expected_weight = sum(
                [weights[i] * expected_sol[i] for i in range(len(weights))]
            )

            if expected_weight > capacity + 1e-6:
                print(
                    "Error: Expected weight ({}) exceeds capacity ({})".format(
                        expected_weight, capacity
                    )
                )

            # Run the solver
            cmd = "./knapsack01 -q {}".format(instance)
            start_time = time.time()
            result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE)
            end_time = time.time()
            elapsed_time = end_time - start_time

            computed_sol = re.split("[ \t\r\n]+", result.stdout.decode())
            computed_sol = [int(v) for v in computed_sol if v != ""]
            computed_profit = sum(
                [profits[i] * computed_sol[i] for i in range(len(profits))]
            )
            computed_weight = sum(
                [weights[i] * computed_sol[i] for i in range(len(profits))]
            )

            if computed_weight > capacity + 1e-6:
                print(
                    "Error: Computed weight ({}) exceeds capacity ({})".format(
                        computed_weight, capacity
                    )
                )

            if abs(computed_profit - expected_profit) < 1e-6:
                print("OK ({:.2f} s)".format(elapsed_time))
            else:
                print(
                    "Error: Computed and expected profits are different (computed={}, expected={})".format(
                        computed_profit, expected_profit
                    )
                )


test(instance_filename, solution_filename)

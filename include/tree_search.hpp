/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TREE_SEARCH_HPP
#define TREE_SEARCH_HPP

#include "node.hpp"
#include "signal.hpp"
#include <functional>
#include <memory>
#include <optional>
#include <vector>

class _TreeSearch_Events;
class _TreeSearch_Inserter;
class _TreeSearch_BranchConcept;
template <typename BranchT, typename NodeT = typename BranchT::Node>
class _TreeSearch_BranchModel;
class _TreeSearch_SearchConcept;
template <typename SearchT>
class _TreeSearch_SearchModel;
class _TreeSearch_ContextConcept;
template <typename ContextT, typename NodeT = typename ContextT::Node>
class _TreeSearch_ContextModel;

/**
 * Tree search algorithm.
 */
class TreeSearch
{
    friend class _TreeSearch_Inserter;

public:
    /**
     * Events type definition.
     */
    typedef struct
    {
        Signal<void(TreeSearch &)> init;
        Signal<void(TreeSearch &)> exit;
        Signal<void(TreeSearch &, const NodeBase &)> node_selected;
        Signal<void(TreeSearch &, const NodeBase &)> node_pushed;
        Signal<void(TreeSearch &, const NodeBase &)> node_candidate;
        Signal<void(TreeSearch &, const NodeBase &)> node_pruned;
        Signal<void(TreeSearch &, const NodeBase &)> node_terminal;
        Signal<void(TreeSearch &, const NodeBase &)> node_improving;
    } Events;

    /**
     * Events definition.
     */
    Events events;

    /**
     * Node inserter type definition.
     */
    typedef _TreeSearch_Inserter Inserter;

    /**
     * Pointer to the context.
     */
    typedef std::shared_ptr<_TreeSearch_ContextConcept> ContextPtr;

    /**
     * Node pool type definition.
     */
    typedef std::vector<NodePtr> NodePool;

    /**
     * Get actual number of nodes.
     */
    std::size_t n_nodes() const;

    /**
     * Get the total node count.
     */
    std::size_t node_count() const;

    /**
     * Get context.
     */
    ContextPtr context() const;

    /**
     * Get best solutions.
     */
    const NodePool &best() const;

    /**
     * Run the algorithm loop.
     */
    void run();

    /**
     * Set context.
     */
    template <typename ContextT>
    void set_context(const ContextT &ctx)
    {
        _ctx = std::make_shared<_TreeSearch_ContextModel<std::decay_t<ContextT>>>(ctx);
    }

    /**
     * Set context.
     */
    template <typename ContextT>
    void set_context(ContextT &&ctx)
    {
        _ctx = std::make_shared<_TreeSearch_ContextModel<std::decay_t<ContextT>>>(std::move(ctx));
    }

    /**
     * Set branching rule.
     */
    template <typename BranchT>
    void set_branch_rule(const BranchT &branch)
    {
        _branch = std::make_unique<_TreeSearch_BranchModel<std::decay_t<BranchT>>>(branch);
    }

    /**
     * Set branching rule.
     */
    template <typename BranchT>
    void set_branch_rule(BranchT &&branch)
    {
        _branch = std::make_unique<_TreeSearch_BranchModel<std::decay_t<BranchT>>>(std::move(branch));
    }

    /**
     * Set search rule.
     */
    template <typename SearchT>
    void set_search_rule(const SearchT &search)
    {
        _search = std::make_unique<_TreeSearch_SearchModel<std::decay_t<SearchT>>>(search);
    }

    /**
     * Set search rule.
     */
    template <typename SearchT>
    void set_search_rule(SearchT &&search)
    {
        _search = std::make_unique<_TreeSearch_SearchModel<std::decay_t<SearchT>>>(std::move(search));
    }

private:
    // Add a node to the queue.
    void _push(NodePtr node);

    std::unique_ptr<_TreeSearch_BranchConcept> _branch;
    std::unique_ptr<_TreeSearch_SearchConcept> _search;
    ContextPtr _ctx;
    NodePool _best;
    std::size_t _node_count = 0;
};

////////////////////////////////////////////////////////////////////////////////

// Node inserter functor.
class _TreeSearch_Inserter
{
public:
    _TreeSearch_Inserter(TreeSearch &ts)
        : _ts(ts)
    {
    }

    _TreeSearch_Inserter(TreeSearch &ts, const NodeBase &parent)
        : _ts(ts),
          _parent(parent)
    {
    }

    template <typename NodeT>
    void operator()(const NodeT &node)
    {
        auto ptr = make_node_ptr(node);
        if (_parent)
        {
            ptr->set_depth(_parent.value().get().depth() + 1);
        }
        _ts._push(std::move(ptr));
    }

    template <typename NodeT>
    void operator()(NodeT &&node)
    {
        auto ptr = make_node_ptr(std::move(node));
        if (_parent)
        {
            ptr->set_depth(_parent.value().get().depth() + 1);
        }
        _ts._push(std::move(ptr));
    }

private:
    TreeSearch &_ts;
    std::optional<std::reference_wrapper<const NodeBase>> _parent;
};

// Type erasure concept for branching rules.
class _TreeSearch_BranchConcept
{
public:
    virtual ~_TreeSearch_BranchConcept() = default;
    virtual void initialize(TreeSearch &ts) = 0;
    virtual void operator()(const NodeBase &parent, TreeSearch::Inserter inserter) = 0;
};

// Type erasure model for branching rules.
template <typename BranchT, typename NodeT>
class _TreeSearch_BranchModel : public _TreeSearch_BranchConcept
{
public:
    _TreeSearch_BranchModel(const BranchT &branch)
        : _branch(branch)
    {
    }

    _TreeSearch_BranchModel(BranchT &&branch)
        : _branch(std::move(branch))
    {
    }

    virtual void initialize(TreeSearch &ts)
    {
        _branch.initialize(ts);
    }

    virtual void operator()(const NodeBase &parent, TreeSearch::Inserter inserter)
    {
        _branch(parent.as<NodeT>(), inserter);
    }

private:
    BranchT _branch;
};

// Type erasure concept for search rules.
class _TreeSearch_SearchConcept
{
public:
    virtual ~_TreeSearch_SearchConcept() = default;
    virtual void initialize(TreeSearch &ts) = 0;
    virtual bool empty() const = 0;
    virtual std::size_t size() const = 0;
    virtual NodePtr pop() = 0;
    virtual void push(NodePtr node) = 0;
    virtual void filter(const NodeBase &node) = 0;
};

// Type erasure model for search rules.
template <typename SearchT>
class _TreeSearch_SearchModel : public _TreeSearch_SearchConcept
{
public:
    _TreeSearch_SearchModel(const SearchT &search)
        : _search(search)
    {
    }

    _TreeSearch_SearchModel(SearchT &&search)
        : _search(std::move(search))
    {
    }

    virtual void initialize(TreeSearch &ts)
    {
        _search.initialize(ts);
    }

    virtual bool empty() const
    {
        return _search.empty();
    }

    virtual std::size_t size() const
    {
        return _search.size();
    }

    virtual NodePtr pop()
    {
        return _search.pop();
    }

    virtual void push(NodePtr node)
    {
        _search.push(std::move(node));
    }

    virtual void filter(const NodeBase &node)
    {
        _search.filter(node);
    }

private:
    SearchT _search;
};

// Type erasure concept for context.
class _TreeSearch_ContextConcept
{
public:
    virtual ~_TreeSearch_ContextConcept() = default;
    virtual void start(TreeSearch::Inserter inserter) = 0;
    virtual bool is_valid(const NodeBase &node) const = 0;
    virtual bool is_terminal(const NodeBase &node) const = 0;
    virtual bool compare(const NodeBase &node1, const NodeBase &node2) const = 0;
    virtual bool dominates(const NodeBase &node1, const NodeBase &node2) const = 0;
    virtual void print(const NodeBase &node, std::ostream &os) const = 0;

    template <typename ContextT>
    const ContextT &as() const
    {
        return *static_cast<const ContextT *>(_cptr());
    }

protected:
    virtual const void *_cptr() const = 0;
};

// Type erasure model for context.
template <typename ContextT, typename NodeT>
class _TreeSearch_ContextModel : public _TreeSearch_ContextConcept
{
public:
    _TreeSearch_ContextModel(const ContextT &ctx)
        : _ctx(ctx)
    {
    }

    _TreeSearch_ContextModel(ContextT &&ctx)
        : _ctx(std::move(ctx))
    {
    }

    virtual void start(TreeSearch::Inserter inserter)
    {
        _ctx.start(inserter);
    }

    virtual bool is_valid(const NodeBase &node) const
    {
        return _ctx.is_valid(node.as<NodeT>());
    }

    virtual bool is_terminal(const NodeBase &node) const
    {
        return _ctx.is_terminal(node.as<NodeT>());
    }

    virtual bool compare(const NodeBase &node1, const NodeBase &node2) const
    {
        return _ctx.compare(node1.as<NodeT>(), node2.as<NodeT>());
    }

    virtual bool dominates(const NodeBase &node1, const NodeBase &node2) const
    {
        return _ctx.dominates(node1.as<NodeT>(), node2.as<NodeT>());
    }

    virtual void print(const NodeBase &node, std::ostream &os) const
    {
        _ctx.print(node.as<NodeT>(), os);
    }

protected:
    virtual const void *_cptr() const
    {
        return &_ctx;
    }

private:
    ContextT _ctx;
};

#endif

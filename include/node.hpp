/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NODE_HPP
#define NODE_HPP

#include <memory>

/**
 * Base class for tree search nodes.
 */
class NodeBase
{
    // Provide access to set_index and set_depth methods.
    friend class TreeSearch;
    friend class _TreeSearch_Inserter;

public:
    /**
     * Destructor.
     */
    virtual ~NodeBase()
    {
    }

    /**
     * Get the unique index of the node.
     */
    std::size_t index() const
    {
        return _index;
    }

    /**
     * Get the depth of the node.
     */
    std::size_t depth() const
    {
        return _depth;
    }

    /**
     * Cast the node to a derived class.
     */
    template <typename NodeT>
    const NodeT &as() const
    {
        return dynamic_cast<const NodeT &>(*this);
    }

    /**
     * Cast the node to a derived class.
     */
    template <typename NodeT>
    NodeT &as()
    {
        return dynamic_cast<NodeT &>(*this);
    }

private:
    /**
     * Set the unique index of the node.
     */
    void set_index(std::size_t i)
    {
        _index = i;
    }

    /**
     * Set the depth of the node.
     */
    void set_depth(std::size_t d)
    {
        _depth = d;
    }

    std::size_t _index = 0;
    std::size_t _depth = 0;
};

/**
 * Node pointer type definition.
 */
typedef std::unique_ptr<NodeBase> NodePtr;

/**
 * Copy a node and create a pointer to it.
 */
template <typename NodeT>
inline NodePtr make_node_ptr(const NodeT &node)
{
    return std::make_unique<NodeT>(node);
}

/**
 * Move a node and create a pointer to it.
 */
template <typename NodeT>
inline NodePtr make_node_ptr(NodeT &&node)
{
    return std::make_unique<NodeT>(std::move(node));
}

#endif

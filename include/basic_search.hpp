/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BASIC_SEARCH_HPP
#define BASIC_SEARCH_HPP

#include "node.hpp"
#include "tree_search.hpp"
#include <set>

/**
 * Base class for basic search strategies such as BFS, BrFS and DFS.
 */
template <typename CompareF>
class BasicSearch
{
public:
    /**
     * Comparator type definition.
     */
    typedef CompareF Compare;

    /**
     * Container type definition.
     */
    typedef std::set<NodePtr, Compare> Container;

    /**
     * Pointer type definition.
     */
    typedef typename Container::iterator Pointer;

    /**
     * Default constructor.
     */
    BasicSearch() = default;

    /**
     * Constructor.
     */
    BasicSearch(TreeSearch &ts)
    {
        initialize(ts);
    }

    /**
     * Initialize the container.
     */
    void initialize(TreeSearch &ts)
    {
        _queue = Container(Compare(ts));
        _ctx = ts.context();
    }

    /**
     * Check whether the queue is empty.
     */
    bool empty() const
    {
        return _queue.empty();
    }

    /**
     * Get the current size of the queue.
     */
    std::size_t size() const
    {
        return _queue.size();
    }

    /**
     * Select the highest priority node.
     */
    NodePtr pop()
    {
        auto it = _queue.begin();
        return std::move(_queue.extract(it).value());
    }

    /**
     * Add a node to the queue.
     */
    void push(NodePtr node)
    {
        _queue.insert(std::move(node));
    }

    /**
     * Filter nodes dominated by the given one.
     */
    void filter(const NodeBase &node)
    {
        for (auto it = std::begin(_queue); it != std::end(_queue);)
        {
            if (_ctx->dominates(node, **it))
            {
                it = _queue.erase(it);
            }
            else
            {
                ++it;
            }
        }
    }

private:
    Container _queue;
    TreeSearch::ContextPtr _ctx;
};

#endif

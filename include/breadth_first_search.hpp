/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BREADTH_FIRST_SEARCH_HPP
#define BREADTH_FIRST_SEARCH_HPP

#include "basic_search.hpp"
#include "node.hpp"
#include "tree_search.hpp"
#include <memory>

/**
 * Comparator for Breadth First Search.
 */
class _BreadthFirstSearchCompare
{
public:
    _BreadthFirstSearchCompare() {}
    _BreadthFirstSearchCompare(TreeSearch &ts)
        : _ctx(ts.context()) {}

    bool operator()(const NodePtr &node1, const NodePtr &node2) const
    {
        return node1->depth() < node2->depth() ||
               (node1->depth() == node2->depth() &&
                (_ctx->compare(*node1, *node2) ||
                 (!_ctx->compare(*node2, *node1) &&
                  node1->index() < node2->index())));
    }

private:
    TreeSearch::ContextPtr _ctx;
};

/**
 * Breadth First Search.
 */
typedef BasicSearch<_BreadthFirstSearchCompare> BreadthFirstSearch;

#endif

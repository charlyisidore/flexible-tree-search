/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CYCLIC_BEST_FIRST_SEARCH_HPP
#define CYCLIC_BEST_FIRST_SEARCH_HPP

#include "node.hpp"
#include "tree_search.hpp"
#include <deque>
#include <set>

/**
 * Comparator for Cyclic Best First Search.
 */
class _CyclicBestFirstSearchCompare
{
public:
    _CyclicBestFirstSearchCompare() {}
    _CyclicBestFirstSearchCompare(TreeSearch::ContextPtr ctx)
        : _ctx(ctx) {}

    bool operator()(const NodePtr &node1, const NodePtr &node2) const
    {
        return _ctx->compare(*node1, *node2) ||
               (!_ctx->compare(*node2, *node1) &&
                (node1->index() < node2->index()));
    }

private:
    TreeSearch::ContextPtr _ctx;
};

/**
 * Cyclic Best First Search.
 */
class CyclicBestFirstSearch
{
public:
    /**
     * Comparator type definition.
     */
    typedef _CyclicBestFirstSearchCompare Compare;

    /**
     * Container type definition.
     */
    typedef std::deque<std::set<NodePtr, Compare>> Container;

    /**
     * Default constructor.
     */
    CyclicBestFirstSearch() = default;

    /**
     * Constructor.
     */
    CyclicBestFirstSearch(TreeSearch &ts)
    {
        initialize(ts);
    }

    /**
     * Initialize the container.
     */
    void initialize(TreeSearch &ts)
    {
        _queue.clear();
        _ctx = ts.context();
        _depth = 0;
    }

    /**
     * Check whether the queue is empty.
     */
    bool empty() const
    {
        for (const auto &q : _queue)
        {
            if (!q.empty())
            {
                return false;
            }
        }
        return true;
    }

    /**
     * Get the current size of the queue.
     */
    std::size_t size() const
    {
        std::size_t s = 0;
        for (const auto &q : _queue)
        {
            s += q.size();
        }
        return s;
    }

    /**
     * Select the highest priority node.
     */
    NodePtr pop()
    {
        if (_depth >= _queue.size() || _queue[_depth].empty())
        {
            _depth = 0;
        }

        while (_depth < _queue.size() && _queue[_depth].empty())
        {
            ++_depth;
        }

        auto it = _queue[_depth].begin();
        return std::move(_queue[_depth++].extract(it).value());
    }

    /**
     * Add a node to the queue.
     */
    void push(NodePtr node)
    {
        // Add new levels if required
        while (node->depth() >= _queue.size())
        {
            _queue.emplace_back(Compare(_ctx));
        }
        _queue[node->depth()].insert(std::move(node));
    }

    /**
     * Filter nodes dominated by the given one.
     */
    void filter(const NodeBase &node)
    {
        for (auto &q : _queue)
        {
            for (auto it = std::begin(q); it != std::end(q);)
            {
                if (_ctx->dominates(node, **it))
                {
                    it = q.erase(it);
                }
                else
                {
                    ++it;
                }
            }
        }
    }

private:
    Container _queue;
    TreeSearch::ContextPtr _ctx;
    std::size_t _depth = 0;
};

#endif

#!/bin/sh
find include/ src/ examples/ -type f \( -name '*.cpp' -o -name '*.hpp' \) -exec clang-format --verbose -i '{}' \;
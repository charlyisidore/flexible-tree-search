/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "tree_search.hpp"

std::size_t TreeSearch::n_nodes() const
{
    return _search->size();
}

std::size_t TreeSearch::node_count() const
{
    return _node_count;
}

TreeSearch::ContextPtr TreeSearch::context() const
{
    return _ctx;
}

const TreeSearch::NodePool &TreeSearch::best() const
{
    return _best;
}

void TreeSearch::run()
{
    // Initialize every component
    _search->initialize(*this);
    _branch->initialize(*this);

    // Notify that everything is initialized
    events.init(*this);

    // Insert root node
    _ctx->start(Inserter(*this));

    // Start loop
    while (!_search->empty())
    {
        // Extract the top-priority node
        auto parent = _search->pop();

        // Notify that a node is selected for branching
        events.node_selected(*this, *parent);

        // Create child nodes
        (*_branch)(*parent, Inserter(*this, *parent));
    }

    events.exit(*this);
}

void TreeSearch::_push(NodePtr node)
{
    // Each node is assigned a unique index
    node->set_index(_node_count++);

    events.node_pushed(*this, *node);

    // Promising node
    if (_best.empty() || !_ctx->dominates(*(_best.front()), *node))
    {
        // Node is a leaf
        if (_ctx->is_terminal(*node))
        {
            events.node_terminal(*this, *node);

            if (!_best.empty())
            {
                for (const auto &n : _best)
                {
                    events.node_pruned(*this, *n);
                }
                _best.clear();
            }

            // Remove dominated nodes
            _search->filter(*node);

            // New best-so-far node
            events.node_improving(*this, *node);
            _best.push_back(std::move(node));
        }
        // Promising node having child nodes
        else
        {
            events.node_candidate(*this, *node);
            _search->push(std::move(node));
        }
    }
    // Dominated node
    else
    {
        events.node_pruned(*this, *node);
    }
}